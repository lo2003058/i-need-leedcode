/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
const search = function (nums, target) {
  let res = -1;
  let [l, r] = [0, nums.length - 1];

  while (l <= r) {
    let m = Math.floor((l + r) / 2);
    if (nums[m] === target) return m;

    if (nums[l] <= nums[m]) {
      if (target > nums[m] || target < nums[l]) {
        l = m + 1;
      }else{
        r = m - 1;
      }
    } else {
      if (target < nums[m] || target > nums[r]) {
        r = m - 1;
      }else{
        l = m + 1;
      }
    }
  }
  return res;
};

console.log(search([4, 5, 6, 7, 0, 1, 2], 5));




