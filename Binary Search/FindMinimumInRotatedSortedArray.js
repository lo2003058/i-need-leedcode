/**
 * @param {number[]} nums
 * @return {number}
 */
const findMin = function (nums) {
  let [l, r] = [0, nums.length - 1];

  while (l < r) {
    const m = Math.floor((l + r) / 2);

    if (nums[l] < nums[r]) return nums[l];

    if (nums[l] <= nums[m]) {
      l = m + 1;
    } else {
      r = m;
    }
  }

  return nums[l];
};

console.log(findMin([2, 3, 4, 5, 1]));
