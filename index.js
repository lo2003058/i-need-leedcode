/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {boolean}
 */
const hasCycle = function (head) {
  let set = new Set();

  while (head !== null) {
    set.add(head);
    head = head.next;
  }

  console.log(set);

};

console.log(hasCycle([3, 2, 0, -4]));
