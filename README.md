# My LeetCode Progress

This repository tracks my progress and solutions to problems on LeetCode.

## Completed Problems

| Problem | Difficulty |Solution | Status     |
|---------|------------|----------|------------|
| Contains Duplicate | Easy | [Link to Solution](https://gitlab.com/lo2003058/i-need-leedcode/-/blob/main/Arrays%20&%20Hashing/ContainsDuplicate.js?ref_type=heads) | Done       |
| Valid Anagram | Easy | [Link to Solution](https://gitlab.com/lo2003058/i-need-leedcode/-/blob/main/Arrays%20&%20Hashing/ValidAnagram.js?ref_type=heads) | Done       |
| Two Sum | Easy |  | Processing |
