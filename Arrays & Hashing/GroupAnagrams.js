/**
 * @param {string[]} strs
 * @return {string[][]}
 */
const groupAnagrams = function (strs) {
  if (strs.length <= 0) return [];

  let hashMap = {};
  // [ 'aet', 'aet', 'ant', 'aet', 'ant', 'abt' ]
  let sorted = strs.map((str) => str.split("").sort().join(""));

  for (let i = 0; i < sorted.length; i++) {
    if (!hashMap[sorted[i]]) {
      hashMap[sorted[i]] = [strs[i]];
    } else {
      hashMap[sorted[i]].push(strs[i]);
    }
  }

  return Object.values(hashMap);
};

groupAnagrams(["eat", "tea", "tan", "ate", "nat", "bat"]);
