/**
 * @param {number[]} nums
 * @return {number[]}
 */
const productExceptSelf = function (nums) {
  let answer = [];
  let prefix = 1;
  let postfix = 1;

  for (let i = 0; i < nums.length; i++) {
    answer[i] = prefix;
    prefix *= nums[i];
  }

  for (let j = nums.length - 1; j >= 0; j--) {
    answer[j] *= postfix;
    postfix = postfix * nums[j];
  }

  return answer;
};

console.log(productExceptSelf([1, 2, 3, 4]));
