/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
const twoSum = function (nums, target) {

  if (nums.length <= 0) {
    return [];
  }
  let numHash = {};
  for (let i = 0; i < nums.length; i++) {
    if (numHash[target - nums[i]] !== undefined) {
      return [numHash[target - nums[i]], i]
    }
    numHash[nums[i]] = i;
  }
  return [];
};


twoSum([3, 2, 4], 6);
