class Solution {
  /**
   * @param {string[]} strs
   * @returns {string}
   */
  encode(strs) {
    let result = "";
    for (let s of strs) {
      result += `${s.length}#${s}`;
    }
    return result;
  }

  /**
   * @param {string} str
   * @returns {string[]}
   */
  decode(str) {
    let result = [];
    let i = 0;
    while (i < str.length) {
      let j = i;
      while (str[j] !== "#") {
        j++;
      }
      let strLength = parseInt(str.substring(i, j));

      // now on first letter, index 1
      i = j + 1;

      j = i + strLength

      result.push(str.substring(i, j));

      i = j;

    }

    return result;

  }
}

const main = new Solution();

console.log("encode:", main.encode(["neet", "code", "love", "you"]));
console.log("decode:", main.decode(main.encode(["neet", "code", "love", "you"])));
