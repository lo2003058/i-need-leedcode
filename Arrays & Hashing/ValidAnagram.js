/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
const isAnagram = function (s, t) {
  if (s.length !== t.length) {
    return false;
  }

  let stringMap = {};

  for (let i = 0; i < s.length; i++) {
    if (stringMap[s[i]] === undefined) {
      stringMap[s[i]] = 1;
    } else {
      stringMap[s[i]] += 1;
    }
  }

  for (let j = 0; j < t.length; j++) {
    if (stringMap[t[j]] === undefined) {
      return false;
    }
    if (stringMap[t[j]] < 1) {
      return false;
    }
    stringMap[t[j]] -= 1;
  }
  return true;
};


isAnagram("anagram", "nagaram");
