/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
const topKFrequent = function (nums, k) {
  let map = {};

  let bucket = [];
  let result = [];

  for (let i = 0; i < nums.length; i++) {
    if (!map[nums[i]]) {
      map[nums[i]] = 1;
    } else {
      map[nums[i]]++;
    }
  }

  for (let [num, freq] of Object.entries(map)) {
    if (!bucket[freq]) {
      bucket[freq] = new Set().add(num);
    } else {
      bucket[freq] = bucket[freq].add(num);
    }
  }

  for (let j = bucket.length - 1; j >= 0; j--) {
    if(bucket[j]) result.push(...bucket[j])
    if(result.length === k) break;
  }

  return result;
};

console.log(topKFrequent([1, 4, 4, 4, 3, 2, 2, 1, 1, 1], 4));
