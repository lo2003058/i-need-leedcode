/**
 * @param {number[]} nums
 * @return {boolean}
 */
const containsDuplicate = function (nums) {
  if (nums.length <= 0) {
    return false;
  }

  let numsHash = {};

  for (let i = 0; i < nums.length; i++) {
    if (numsHash[nums[i]] !== undefined) {
      return true;
    }
    numsHash[nums[i]] = i;
  }

  return false;
};

containsDuplicate([1, 2, 3, 4]);
