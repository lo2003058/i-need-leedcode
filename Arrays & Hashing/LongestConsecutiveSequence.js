/**
 * @param {number[]} nums
 * @return {number}
 */
const longestConsecutive = function (nums) {
  if (nums == null || nums.length === 0) {
    return 0;
  }

  let set = new Set(nums);
  let max = 0;

  for (const number of set) {
    if (set.has(number - 1)) {
      continue;
    }
    let currNum = number;
    let currSeq = 1;

    while (set.has(currNum + 1)) {
      currNum++;
      currSeq++;
    }
    max = Math.max(max, currSeq);
  }

  return max;
};
