/**
 * @param {string} s
 * @return {boolean}
 */
const isValid = function (s) {
  let stack = [];
  const closeToOpen = {
    '}': '{',
    ']': '[',
    ')': '(',
  }

  for (const c of s) {
    if (!(c in closeToOpen)) {
      stack.push(c);
      continue;
    }
    if (stack[stack.length - 1] === closeToOpen[c]) {
      stack.pop();
      continue;
    }
    return false;
  }
  return stack.length === 0;
};

console.log(isValid("()[]{}"));
