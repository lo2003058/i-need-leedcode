/**
 * @param {number[]} prices
 * @return {number}
 */
const maxProfit = function (prices) {
  let l = 0;
  let r = 1;
  let maxProfit = 0;

  while (r < prices.length) {
    if (prices[l] < prices[r]) {
      if(prices[r] - prices[l] > maxProfit) maxProfit = prices[r] - prices[l];
    } else {
      l = r;
    }
    r++;
  }
  return maxProfit
};


console.log(maxProfit([7, 1, 5, 3, 6, 4]));
