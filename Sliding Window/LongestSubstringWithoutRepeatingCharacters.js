/**
 * @param {string} string
 * @return {number}
 */
const lengthOfLongestSubstring = function (string) {
  if (string.length <= 0) return 0;
  let l = 0;
  let length = 0;
  const set = new Set();

  for (let r = 0; r < string.length; r++) {
    while (set.has(string[r])) {
      set.delete(string[l]);
      l++;
    }
    set.add(string[r]);
    length = Math.max(length, r - l + 1);
  }
  return length;
};

console.log(lengthOfLongestSubstring("pwwkew"));
