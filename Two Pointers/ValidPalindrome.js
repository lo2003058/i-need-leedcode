/**
 * @param {string} string
 * @return {boolean}
 */
const isPalindrome = function (string) {
  if (string.length <= 0) return false;

  let formatedString = string.replace(/[^a-z0-9]/gi, '').toLowerCase();
  let l = 0;
  let r = formatedString.length - 1;

  console.log("formatedString:", formatedString);

  for (let s of formatedString) {
    if (formatedString[l] !== formatedString[r]) {
      return false;
    } else {
      l++;
      r--;
    }
  }
  return true;
};

