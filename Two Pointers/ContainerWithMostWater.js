/**
 * @param {number[]} height
 * @return {number}
 */
const maxArea = function (height) {
  let maxArea = 0;
  let l = 0;
  let r = height.length - 1;
  for (let i = 0; i < height.length; i++) {
    let distance = r - l;
    let minHeight = Math.min(height[r], height[l]);
    maxArea = Math.max(maxArea, distance * minHeight);

    if (height[r] > height[l]) {
      l++;
    } else if (height[l] >= height[r]) {
      r--;
    }
  }
  return maxArea;
};

