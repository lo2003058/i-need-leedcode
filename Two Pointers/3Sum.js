/**
 * @param {number[]} nums
 * @return {number[][]}
 */
const threeSum = function (nums) {
  let res = [];
  nums.sort((a, b) => a - b);

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] > 0) break;
    if (i > 0 && nums[i] === nums[i - 1]) {
      continue;
    }

    let l = i + 1;  // for sum < 0;
    let r = nums.length - 1;  // for sum > 0;

    while (l < r) {
      const sum = nums[i] + nums[l] + nums[r];
      if (sum > 0) {
        r--;
      } else if (sum < 0) {
        l++;
      } else {
        res.push([nums[i], nums[l], nums[r]]);
        l++;
        r--;
        while (nums[l] === nums[l - 1] && l < r) {
          l++;
        }

      }
    }

  }
  return res;
};

// [[-1,-1,2],[-1,0,1]]
// console.log(threeSum([-1, 0, 1, 2, -1, -4]));

// [[0,0,0]]
// console.log(threeSum([0, 0, 0, 0]));

// [[0,0,0]]
// console.log(threeSum([0, 0, 0]));

// [[-2,0,2]]
console.log(threeSum([-2, 0, 0, 2, 2]));
